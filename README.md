# DNS Server Updater

This is a sample PowerShell script, that you can use for inspiration to write you own script.
Currenlty still a work in progress but the idea is to handle the update of DNS records.

The script uses PowerShell to get DNS records from Public DNS server writes to a CSV file, queries AD DNS, writes to CSV and then compares the two files, if public DNS has changed, it updates the local Active Directory DNS server record. This is useful in cases where the application cannot use a private IP and we want to prevent hairpin NAT.

Please do not copy and paste without carefully reading the code and please test this in a non production environment as this script does edit DNS records. Please use at your own risk. 