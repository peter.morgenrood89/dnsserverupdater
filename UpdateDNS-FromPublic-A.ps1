# Updates A records only
# Edit Variables below:
# PATH TO STORE LOCAL AD DNS RECORDS FOR COMPARE
$MyRecordPath = "C:\records.csv"
# PATH TO STORE PUBLIC DNS RECORDS
$MyUpdatedCSVPath = "C:\updatedDNS.csv"
# PUBLIC DNS IP i.e AT&T
$MyDNSServerIP = "1.1.1.1"
# LOCAL DNS IP FOR AD 
$ADDNSServerIP = "172.31.10.10"
# FQDN OF THE RECORD YOU TRYING TO MONITOR
$DomainName = "cp.somedomain.co.za "
######################################
#DO NOT EDIT AFTER THIS LINE
######################################

# Install the required PowerShell Active Directory module
 Write-Output "Checking if need to install AD powershell and DNS tools" 
 $DNSfeature = Get-WindowsFeature -Name RSAT-DNS-Server  
 if($DNSfeature.InstallState -ne "Installed"){
     Install-WindowsFeature RSAT-DNS-Server
     Write-Output "DNS tools installed successfully"
 } 
 $PSfeature = Get-WindowsFeature -Name RSAT-AD-PowerShell 
 if($PSfeature.InstallState -ne "Installed"){
     Install-WindowsFeature RSAT-AD-PowerShell
     Write-Output "AD PowerShell tools installed successfully"
 }
# PREPARE FUNCTIONS
Function Get-DNS ($DNSServerIP, $Type, $RecordPath){
    Write-Output "Creating DNS CSV" $RecordPath
    $Resolve = Resolve-DnsName -Name $DomainName -Server $DNSServerIP -Type $Type 
    $Resolve | Export-CSV -path $RecordPath -Force
    Write-Output $Resolve
}
Function Compare-Properties($existing, $new) {
    # => - Difference in destination object.
    # <= - Difference in reference (source) object.
    # == - When the source and destination objects are equal.#
    $mismatch = $false
    Compare-Object -ReferenceObject $existing -DifferenceObject $new -SyncWindow 0 -Property Address
    if ((Compare-Object -ReferenceObject $existing -DifferenceObject $new -SyncWindow 0 -Property Address).Length -ne 0) {
        $mismatch = $true
    } 
    Write-Output "DNS record compare match is" $mismatch
    return $mismatch
}
# Get DNS from public DNS and store it in a csv
Get-DNS -DNSServerIP $MyDNSServerIP -Type A -RecordPath $MyRecordPath

# If we have existing csv file, create new compare file
$TestDest = Test-Path -Path $RecordPath
if ($TestDest){
    Get-DNS -DNSServerIP $MyDNSServerIP -Type A -RecordPath $MyUpdatedCSVPath
}

# Import the records from CSV, and compare
Clear-Variable -Name ExistingCSV
Clear-Variable -Name UpdatedCSV
$ExistingCSV = Import-CSV -Path $RecordPath
$UpdatedCSV = Import-CSV -Path $UpdatedCSVPath
# COMPARE SOURCE CSV WITH ONE FROM PUBLIC DNS  
$Compare = Compare-Properties -existing $ExistingCSV -new $UpdatedCSV
# IF DIFFERENCE UPDATE DNS SERVER
if ($Compare -eq $True){
    Write-Output "Fetch new IP from CSV"
    $NewIP1 = $UpdatedCSV.IP4Address
    #Update DNS with new IP
    $RecName = $DomainName.Split(".")[0]
    $ZoneName=((Get-WmiObject Win32_ComputerSystem).Domain)
    $DnsServerComputerName = (Resolve-DnsName $ZoneName -Type NS | Where Type -eq 'A' | Select -ExpandProperty Name)[0]
    $OldObj = Get-DnsServerResourceRecord -ComputerName $DnsServerComputerName -ZoneName "$ZoneName" -RRType "A" -Name $RecName
    $NewObj = $OldObj.Clone()
    $NewObj.RecordData.IPv4Address = [System.Net.IPAddress]::parse("$NewIP1")
    Set-DnsServerResourceRecord -NewInputObject $NewObj -OldInputObject $OldObj -ZoneName "$ZoneName" -PassThru -ComputerName $DnsServerComputerName
  
}
